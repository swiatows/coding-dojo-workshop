package com.codingdojo.workshop.model;

import com.codingdojo.workshop.model.output.InventoryOutputData;
import com.codingdojo.workshop.util.DojoHelper;
import com.google.gson.JsonObject;
import org.junit.Test;

import static com.codingdojo.workshop.util.DojoHelperTest.verifyJson;

public class InventoryDataTest {

    @Test
    public void calculateCumulativeAtp() throws Exception {

        // given
        InventoryData inventoryData = DojoHelper.readInputJsonData("/part1/small/smallInventoryInput", InventoryData.class);

        // when
        InventoryOutputData itemDateOutputInfo = inventoryData.calculateCumulativeAtp();

        // then
        JsonObject actualJSON = DojoHelper.createOutputJsonData(itemDateOutputInfo, InventoryOutputData.class);
        verifyJson("/part1/small/smallInventoryOutput", actualJSON);
    }
}
