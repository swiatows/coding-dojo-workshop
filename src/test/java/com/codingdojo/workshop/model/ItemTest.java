package com.codingdojo.workshop.model;

import com.codingdojo.workshop.model.output.InventoryOutputData;
import com.codingdojo.workshop.util.DojoHelper;
import com.google.gson.JsonObject;
import org.junit.Test;

import static com.codingdojo.workshop.util.DojoHelperTest.verifyJson;

public class ItemTest {


    @Test
    public void calculateCumulativeAtp_forOneItemWithOneDayWithZeroCurrentInventory() throws Exception {

        // given
        InventoryData inventoryData = DojoHelper.readInputJsonData("/part1/small/smallData1", InventoryData.class);

        // when
        InventoryOutputData itemDateOutputInfo = inventoryData.calculateCumulativeAtp();

        // then
        JsonObject actualJSON = DojoHelper.createOutputJsonData(itemDateOutputInfo, InventoryOutputData.class);
        verifyJson("/part1/small/smallDataOutput1", actualJSON);
    }

    @Test
    public void calculateCumulativeAtp_forOneItemWithOneDayWithCurrentInventory() throws Exception {

        // given
        InventoryData inventoryData = DojoHelper.readInputJsonData("/part1/small/smallData2", InventoryData.class);

        // when
        InventoryOutputData itemDateOutputInfo = inventoryData.calculateCumulativeAtp();

        // then
        JsonObject actualJSON = DojoHelper.createOutputJsonData(itemDateOutputInfo, InventoryOutputData.class);
        verifyJson("/part1/small/smallDataOutput2", actualJSON);
    }

    @Test
    public void calculateCumulativeAtp_forOneItemWithTwoDaysWithCurrentInventory() throws Exception {

        // given
        InventoryData inventoryData = DojoHelper.readInputJsonData("/part1/small/smallData3", InventoryData.class);

        // when
        InventoryOutputData itemDateOutputInfo = inventoryData.calculateCumulativeAtp();

        // then
        JsonObject actualJSON = DojoHelper.createOutputJsonData(itemDateOutputInfo, InventoryOutputData.class);
        verifyJson("/part1/small/smallDataOutput3", actualJSON);
    }
}