package com.codingdojo.workshop.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;

public class ItemDateInfoTest {


    @Test
    public void calculateTotalDemand() {
        // given
        ItemDateInfo itemDateInfo = new ItemDateInfo(LocalDate.now(), 10, 10, 0, 0);

        // when
        int result = itemDateInfo.calculateTotalDemand();

        // then
        Assertions.assertThat(result).isEqualTo(20);
    }

    @Test
    public void calculateTotalSupplies() {
        // given
        ItemDateInfo itemDateInfo = new ItemDateInfo(LocalDate.now(), 10, 10, 10, 10);

        // when
        int result = itemDateInfo.calculateTotalSupplies();

        // then
        Assertions.assertThat(result).isEqualTo(20);
    }

    @Test
    public void calculateAtp_WhenMoreDemandsThanSupplies() {
        // given
        ItemDateInfo itemDateInfo = new ItemDateInfo(LocalDate.now(), 10, 10, 0, 0);

        // when
        int result = itemDateInfo.calculateAtp();

        // then
        Assertions.assertThat(result).isEqualTo(-20);
    }

    @Test
    public void calculateAtp_WhenMoreSuppliesThanDemand() {
        // given
        ItemDateInfo itemDateInfo = new ItemDateInfo(LocalDate.now(), 0, 0, 10, 10);

        // when
        int result = itemDateInfo.calculateAtp();

        // then
        Assertions.assertThat(result).isEqualTo(20);
    }

    @Test
    public void calculateAtp_WhenSuppliesEqualDemand() {
        // given
        ItemDateInfo itemDateInfo = new ItemDateInfo(LocalDate.now(), 10, 10, 10, 10);

        // when
        int result = itemDateInfo.calculateAtp();

        // then
        Assertions.assertThat(result).isEqualTo(0);
    }

}