package com.codingdojo.workshop.util;

import com.google.gson.JsonObject;
import org.junit.Test;

import java.io.UnsupportedEncodingException;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;

public class DojoHelperTest {

    @Test
    public void calculate() throws Exception {

//        // given
//        TestModel testData = DojoHelper.readInputJsonData("/part1/test", TestModel.class);
//
//        // then
//        JsonObject actualJSON = DojoHelper.createOutputJsonData(testData, TestModel.class);
//        verifyJson("/part1/test", actualJSON);
//
//        assertJsonEquals(DojoHelper.convertStringToJSON("{\"test\": 1}"), actualJSON);
    }

    public static void verifyJson(String outputJsonFileResourceName, JsonObject actualJSON) throws UnsupportedEncodingException {
        JsonObject expectedJSON = DojoHelper.convertFileToJSON(outputJsonFileResourceName);
        assertJsonEquals(expectedJSON, actualJSON);
    }
}