package com.codingdojo.workshop.model;

import java.time.LocalDate;

public class ItemDateInfo {

    private LocalDate date;
    private int promised;
    private int dependentDemands;
    private int purchaseOrders;
    private int scheduleProduction;

    public ItemDateInfo(LocalDate date, int promised, int dependentDemands, int purchaseOrders, int scheduleProduction) {
        this.date = date;
        this.promised = promised;
        this.dependentDemands = dependentDemands;
        this.purchaseOrders = purchaseOrders;
        this.scheduleProduction = scheduleProduction;
    }

    int calculateTotalDemand() {
        return promised + dependentDemands;
    }

    int calculateTotalSupplies() {
        return purchaseOrders + scheduleProduction;
    }

    public int calculateAtp() {
        return calculateTotalSupplies() - calculateTotalDemand();
    }

    public LocalDate getDate() {
        return date;
    }
}
