package com.codingdojo.workshop.model.output;

import java.time.LocalDate;

public class ItemDateOutputInfo {

    private LocalDate date;
    private int atp;

    public ItemDateOutputInfo(LocalDate date, int atp) {
        this.date = date;
        this.atp = atp;
    }
}
