package com.codingdojo.workshop.model.output;

import com.google.common.collect.Lists;

import java.util.List;

public class ItemOutputData {

    private int id;
    private List<ItemDateOutputInfo> atps = Lists.newArrayList();

    public ItemOutputData(int id) {
        this.id = id;
    }

    public void addItemDateOutputInfo(ItemDateOutputInfo itemDateOutputInfo) {
        atps.add(itemDateOutputInfo);
    }
}
