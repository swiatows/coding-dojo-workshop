package com.codingdojo.workshop.model.output;

import com.google.common.collect.Lists;

import java.util.List;

public class InventoryOutputData {

    private List<ItemOutputData> items = Lists.newArrayList();

    public void addItemOutputData(ItemOutputData itemOutputData) {
        items.add(itemOutputData);
    }
}
