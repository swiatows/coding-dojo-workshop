package com.codingdojo.workshop.model;

import com.codingdojo.workshop.model.output.InventoryOutputData;

import java.util.List;

public class InventoryData {

    private List<Item> items;

    public InventoryOutputData calculateCumulativeAtp() {
        InventoryOutputData inventoryOutputData = new InventoryOutputData();
        items.stream().map(Item::calculateCumulativeAtp).forEach(inventoryOutputData::addItemOutputData);

        return inventoryOutputData;
    }
}
