package com.codingdojo.workshop.model;

import com.codingdojo.workshop.model.output.ItemDateOutputInfo;
import com.codingdojo.workshop.model.output.ItemOutputData;
import com.google.common.collect.Lists;

import java.util.List;

public class Item {

    private int id;
    private String name;
    private int currentInventory;
    private List<ItemDateInfo> dateInfo;

    public ItemOutputData calculateCumulativeAtp() {
        ItemOutputData itemOutputData = new ItemOutputData(id);

        List<Integer> itemPerDayCumulativeATP = calculateCumulativeATPperDay(calculateATPperDay());

        for (int i = 0; i < dateInfo.size(); i++) {
            itemOutputData.addItemDateOutputInfo(new ItemDateOutputInfo(dateInfo.get(i).getDate(), itemPerDayCumulativeATP.get(i)));
        }

        return itemOutputData;
    }

    private List<Integer> calculateCumulativeATPperDay(List<Integer> itemPerDayATP) {
        List<Integer> itemPerDayCumulativeATP = Lists.newArrayList();
        calculateCumulativeAtpRec(0, itemPerDayATP, itemPerDayCumulativeATP);
        return itemPerDayCumulativeATP;
    }

    private int calculateATP(int element, List<Integer> itemPerDayATP) {
        int itemOneDayAtp = dateInfo.get(element).calculateAtp();

        if (element < dateInfo.size() - 1) {
            itemOneDayAtp = itemOneDayAtp + calculateATP(element + 1, itemPerDayATP);
        }

        if (isCurrentDay(element)) {
            itemOneDayAtp += currentInventory;
        }

        if (itemOneDayAtp < 0) {
            itemPerDayATP.add(0);
            return itemOneDayAtp;
        }

        itemPerDayATP.add(itemOneDayAtp);
        return 0;
    }

    private boolean isCurrentDay(int element) {
        return element == 0;
    }

    private List<Integer> calculateATPperDay() {
        List<Integer> itemPerDayATP = Lists.newArrayList();
        calculateATP(0, itemPerDayATP);
        return itemPerDayATP;
    }

    private int calculateCumulativeAtpRec(int element, List<Integer> itemPerDayATP, List<Integer> itemPerDayCumulativeATP) {
        int cumulativeATPperDay = itemPerDayATP.get(element);

        if (element < itemPerDayATP.size() - 1) {
            cumulativeATPperDay = cumulativeATPperDay + calculateCumulativeAtpRec(element + 1, itemPerDayATP, itemPerDayCumulativeATP);
        }
        itemPerDayCumulativeATP.add(cumulativeATPperDay);

        return cumulativeATPperDay;
    }
}
