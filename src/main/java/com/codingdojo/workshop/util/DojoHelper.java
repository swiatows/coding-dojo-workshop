package com.codingdojo.workshop.util;

import com.codingdojo.workshop.model.output.InventoryOutputData;
import com.codingdojo.workshop.model.output.ItemDateOutputInfo;
import com.codingdojo.workshop.model.output.ItemOutputData;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DojoHelper {

    private static Gson gson = new GsonBuilder()
            .registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
            .enableComplexMapKeySerialization()
            .setPrettyPrinting()
            .create();


    private static JsonParser parser = new JsonParser();

    public static <T> T readInputJsonData(String jsonFileResourceName, Class<T> jsonDataClass) throws UnsupportedEncodingException {
        Reader reader = new InputStreamReader(ClassLoader.class.getResourceAsStream(jsonFileResourceName), StandardCharsets.UTF_8.name());
        return gson.fromJson(reader, jsonDataClass);
    }

    public static <T> String createOutputJsonDataAsString(T primitiveData) {
        return gson.toJson(primitiveData);
    }

    public static <T> String createOutputJsonDataAsString(T data, Class<T> jsonDataClass) {
        return gson.toJson(data, jsonDataClass);
    }

    public static <T> JsonObject createOutputJsonData(T primitiveData) {
        return (JsonObject) parser.parse(createOutputJsonDataAsString(primitiveData));
    }

    public static <T> JsonObject createOutputJsonData(T data, Class<T> jsonDataClass) {
        return (JsonObject) parser.parse(createOutputJsonDataAsString(data, jsonDataClass));
    }

    public static <K, V> Map<K, V> zipToMap(List<K> keys, List<V> values) {
        Iterator<K> keyIter = keys.iterator();
        Iterator<V> valIter = values.iterator();

        return new TreeMap<>(IntStream.range(0, keys.size()).boxed()
                .collect(Collectors.toMap(_i -> keyIter.next(), _i -> valIter.next())));
    }

    public static JsonObject convertFileToJSON(String jsonFileResourceName) throws UnsupportedEncodingException {
        Reader reader = new InputStreamReader(ClassLoader.class.getResourceAsStream(jsonFileResourceName), StandardCharsets.UTF_8.name());
        return parser.parse(reader).getAsJsonObject();
    }

    public static JsonObject convertStringToJSON(String string) {
        return (JsonObject) parser.parse(string);
    }

    public static void writeToFileInResources(JsonObject jsonObject, String fileName) {
        writeToFileInResources(jsonObject.toString(), fileName);
    }

    public static void writeToFileInResources(String fileContent, String fileName) {
        try (FileWriter file = new FileWriter(fileName + ".json")) {
            file.write(fileContent);
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}